-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: 10.17.59.23    Database: globalmoema
-- ------------------------------------------------------
-- Server version	5.5.40-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `src` varchar(255) NOT NULL,
  `title` varchar(110) DEFAULT '',
  `en_title` varchar(110) DEFAULT '',
  `es_title` varchar(110) DEFAULT '',
  `alt` varchar(150) DEFAULT NULL,
  `en_alt` varchar(255) DEFAULT '',
  `es_alt` varchar(255) DEFAULT '',
  `longdesc` text,
  `description` text,
  `en_description` text,
  `es_description` text,
  `link` varchar(255) DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,'f41759df6ec05f956c5488f54ba49157.png','','','','','','','',NULL,NULL,NULL,'',0,'2016-11-04 11:04:34','2016-11-04 11:04:34'),(2,'74303038f951cd49abea9f27d5f17ade.png','','','','','','','',NULL,NULL,NULL,'',1,'2016-11-04 11:05:52','2016-11-04 11:05:52');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `description` text NOT NULL,
  `src` varchar(255) NOT NULL,
  `created` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `certificates`
--

DROP TABLE IF EXISTS `certificates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certificates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `send_txt` tinyint(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `company_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `serial_number` varchar(255) NOT NULL,
  `value_secure` float NOT NULL,
  `value` float DEFAULT NULL,
  `value_sale` float NOT NULL,
  `franchise` float NOT NULL,
  `award_total` float NOT NULL,
  `award_liquid` float NOT NULL,
  `iof` float NOT NULL,
  `accident` float NOT NULL,
  `theft` float NOT NULL,
  `prolabore` float NOT NULL,
  `NF` varchar(20) NOT NULL,
  `serie_NF` varchar(8) NOT NULL,
  `tax` float NOT NULL,
  `model` varchar(255) NOT NULL,
  `mark` varchar(40) NOT NULL,
  `provider_id` int(11) DEFAULT NULL,
  `n_secure` int(11) NOT NULL,
  `n_certificate` varchar(20) NOT NULL,
  `code_client` int(11) NOT NULL,
  `name_client` varchar(200) NOT NULL,
  `mail_client` varchar(180) NOT NULL,
  `cpf_client` varchar(11) NOT NULL,
  `rg_client` varchar(40) NOT NULL,
  `birth` date NOT NULL,
  `ddd_phone` smallint(2) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `phonecel` varchar(15) NOT NULL,
  `zipcode` varchar(11) NOT NULL,
  `address` varchar(220) NOT NULL,
  `number` int(11) NOT NULL,
  `complement` varchar(120) DEFAULT NULL,
  `neighborhood` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `state` varchar(200) NOT NULL,
  `number_card` varchar(25) NOT NULL,
  `description_play` varchar(200) NOT NULL,
  `local_pay` tinyint(4) NOT NULL,
  `pay_type` varchar(4) NOT NULL,
  `pay_receipt` tinyint(4) NOT NULL,
  `pay_flag` varchar(100) NOT NULL,
  `pay_installments` int(11) NOT NULL,
  `value_installments` float NOT NULL,
  `cancel_request` tinyint(4) NOT NULL,
  `description_cancellation` text NOT NULL,
  `file_cancellation` varchar(255) NOT NULL,
  `type_cancellation` tinyint(1) NOT NULL,
  `reason_cancellation` tinyint(1) NOT NULL,
  `date_cancellation` date NOT NULL,
  `final_vigencia` date NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certificates`
--

LOCK TABLES `certificates` WRITE;
/*!40000 ALTER TABLE `certificates` DISABLE KEYS */;
/*!40000 ALTER TABLE `certificates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `news_page_id` int(11) NOT NULL,
  `nome` varchar(220) NOT NULL,
  `email` varchar(200) NOT NULL,
  `mensagem` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE latin1_general_ci DEFAULT '',
  `name_fantasy` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `cnpj` varchar(18) COLLATE latin1_general_ci DEFAULT NULL,
  `type_person` smallint(1) DEFAULT '0',
  `add_product` int(1) DEFAULT '0',
  `provider_id` int(11) DEFAULT '0',
  `ie` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `cpf` varchar(14) COLLATE latin1_general_ci DEFAULT NULL,
  `rg` varchar(12) COLLATE latin1_general_ci DEFAULT NULL,
  `address` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `number` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `address2` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `complement` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `zipcode` varchar(10) COLLATE latin1_general_ci DEFAULT '',
  `city` varchar(100) COLLATE latin1_general_ci DEFAULT '',
  `state` varchar(100) COLLATE latin1_general_ci DEFAULT '',
  `country_id` int(11) DEFAULT NULL,
  `phone` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `phonecel` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `phonefax` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `contact` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `email` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `site` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies_providers`
--

DROP TABLE IF EXISTS `companies_providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies_providers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies_providers`
--

LOCK TABLES `companies_providers` WRITE;
/*!40000 ALTER TABLE `companies_providers` DISABLE KEYS */;
/*!40000 ALTER TABLE `companies_providers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cursos`
--

DROP TABLE IF EXISTS `cursos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cursos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `descricao` text,
  `duracao` varchar(255) DEFAULT NULL,
  `horarios` varchar(255) DEFAULT NULL,
  `investimento` varchar(255) DEFAULT NULL,
  `desconto` varchar(255) DEFAULT NULL,
  `certificados` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `src` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cursos`
--

LOCK TABLES `cursos` WRITE;
/*!40000 ALTER TABLE `cursos` DISABLE KEYS */;
/*!40000 ALTER TABLE `cursos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `downloads_archives`
--

DROP TABLE IF EXISTS `downloads_archives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloads_archives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `downloads_category_id` int(11) NOT NULL,
  `downloads_groups_category_id` int(11) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `en_title` varchar(255) DEFAULT '',
  `es_title` varchar(255) DEFAULT '',
  `subtitle` varchar(255) DEFAULT NULL,
  `en_subtitle` varchar(255) DEFAULT '',
  `es_subtitle` varchar(255) DEFAULT '',
  `content` text,
  `en_content` text,
  `es_content` text,
  `src` varchar(255) DEFAULT '',
  `archive` varchar(255) DEFAULT '',
  `alt` varchar(150) DEFAULT NULL,
  `longdesc` text,
  `ordering` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `idade` varchar(255) DEFAULT NULL,
  `especialidade` varchar(255) DEFAULT NULL,
  `conclusao` varchar(255) DEFAULT NULL,
  `bairro` varchar(255) DEFAULT NULL,
  `cidade` varchar(255) DEFAULT NULL,
  `estado` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `downloads_archives`
--

LOCK TABLES `downloads_archives` WRITE;
/*!40000 ALTER TABLE `downloads_archives` DISABLE KEYS */;
/*!40000 ALTER TABLE `downloads_archives` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `downloads_categories`
--

DROP TABLE IF EXISTS `downloads_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloads_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `downloads_config_id` int(11) NOT NULL,
  `src` varchar(255) DEFAULT '',
  `name` varchar(255) DEFAULT '',
  `en_name` varchar(255) DEFAULT '',
  `es_name` varchar(255) DEFAULT '',
  `subtitle` varchar(255) DEFAULT NULL,
  `en_subtitle` varchar(255) DEFAULT '',
  `es_subtitle` varchar(255) DEFAULT '',
  `content` text,
  `en_content` text,
  `es_content` text,
  `image_width` int(11) DEFAULT '0',
  `image_height` int(11) DEFAULT '0',
  `thumb_width` int(11) DEFAULT NULL,
  `thumb_height` int(11) DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `downloads_categories`
--

LOCK TABLES `downloads_categories` WRITE;
/*!40000 ALTER TABLE `downloads_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `downloads_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `downloads_configs`
--

DROP TABLE IF EXISTS `downloads_configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloads_configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `allow_create_category` tinyint(3) DEFAULT '0',
  `allow_create_group` tinyint(1) DEFAULT '0',
  `allow_categorie_subtitle` tinyint(1) DEFAULT NULL,
  `allow_categorie_content` tinyint(1) DEFAULT NULL,
  `allow_archive_title` tinyint(1) DEFAULT NULL,
  `allow_archive_subtitle` tinyint(1) DEFAULT NULL,
  `allow_archive_content` tinyint(1) DEFAULT NULL,
  `image_width` int(11) DEFAULT '0',
  `image_height` int(11) DEFAULT '0',
  `thumb_width` int(11) DEFAULT '0',
  `thumb_height` int(11) DEFAULT '0',
  `category_status` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `downloads_configs`
--

LOCK TABLES `downloads_configs` WRITE;
/*!40000 ALTER TABLE `downloads_configs` DISABLE KEYS */;
/*!40000 ALTER TABLE `downloads_configs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `downloads_groups_categories`
--

DROP TABLE IF EXISTS `downloads_groups_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloads_groups_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `downloads_category_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `en_name` varchar(200) NOT NULL,
  `es_name` varchar(200) NOT NULL,
  `created` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `downloads_groups_categories`
--

LOCK TABLES `downloads_groups_categories` WRITE;
/*!40000 ALTER TABLE `downloads_groups_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `downloads_groups_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries_albums`
--

DROP TABLE IF EXISTS `galleries_albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries_albums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `galleries_config_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT '',
  `en_name` varchar(255) DEFAULT '',
  `es_name` varchar(255) DEFAULT '',
  `subtitle` varchar(255) DEFAULT NULL,
  `en_subtitle` varchar(255) DEFAULT '',
  `es_subtitle` varchar(255) DEFAULT '',
  `content` text,
  `en_content` text,
  `es_content` text,
  `atributos` text,
  `en_atributos` text,
  `es_atributos` text,
  `image_width` int(11) DEFAULT '0',
  `image_height` int(11) DEFAULT '0',
  `thumb_width` int(11) DEFAULT NULL,
  `thumb_height` int(11) DEFAULT NULL,
  `crop_width` int(11) DEFAULT NULL,
  `crop_height` int(11) DEFAULT NULL,
  `crop_x` varchar(25) DEFAULT NULL,
  `crop_y` varchar(25) DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries_albums`
--

LOCK TABLES `galleries_albums` WRITE;
/*!40000 ALTER TABLE `galleries_albums` DISABLE KEYS */;
/*!40000 ALTER TABLE `galleries_albums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries_configs`
--

DROP TABLE IF EXISTS `galleries_configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries_configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `allow_album_subtitle` tinyint(1) DEFAULT NULL,
  `allow_album_content` tinyint(1) DEFAULT NULL,
  `allow_album_atributos` smallint(1) DEFAULT '0',
  `allow_image_title` tinyint(1) DEFAULT NULL,
  `allow_image_subtitle` tinyint(1) DEFAULT NULL,
  `allow_image_content` tinyint(1) DEFAULT NULL,
  `image_width` int(11) DEFAULT NULL,
  `image_height` int(11) DEFAULT NULL,
  `thumb_width` int(11) DEFAULT NULL,
  `thumb_height` int(11) DEFAULT NULL,
  `crop_width` int(11) DEFAULT NULL,
  `crop_height` int(11) DEFAULT NULL,
  `crop_x` varchar(25) DEFAULT NULL,
  `crop_y` varchar(25) DEFAULT NULL,
  `album_status` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries_configs`
--

LOCK TABLES `galleries_configs` WRITE;
/*!40000 ALTER TABLE `galleries_configs` DISABLE KEYS */;
/*!40000 ALTER TABLE `galleries_configs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries_images`
--

DROP TABLE IF EXISTS `galleries_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `galleries_album_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `en_title` varchar(255) DEFAULT '',
  `es_title` varchar(255) DEFAULT '',
  `subtitle` varchar(255) DEFAULT NULL,
  `en_subtitle` varchar(255) DEFAULT '',
  `es_subtitle` varchar(255) DEFAULT '',
  `content` text,
  `en_content` text,
  `es_content` text,
  `src` varchar(255) DEFAULT '',
  `alt` varchar(150) DEFAULT NULL,
  `longdesc` text,
  `en_longdesc` varchar(255) DEFAULT '',
  `ordering` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries_images`
--

LOCK TABLES `galleries_images` WRITE;
/*!40000 ALTER TABLE `galleries_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `galleries_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `en_name` varchar(255) DEFAULT NULL,
  `src` varchar(255) DEFAULT NULL,
  `description` text,
  `en_description` text,
  `image_width` int(11) DEFAULT NULL,
  `image_height` int(11) DEFAULT NULL,
  `thumb_width` int(11) DEFAULT NULL,
  `thumb_height` int(11) DEFAULT NULL,
  `crop_width` int(11) DEFAULT NULL,
  `crop_height` int(11) DEFAULT NULL,
  `crop_x` varchar(20) DEFAULT NULL,
  `crop_y` varchar(20) DEFAULT NULL,
  `alt` varchar(150) DEFAULT NULL,
  `en_alt` varchar(150) DEFAULT NULL,
  `longdesc` text,
  `en_longdesc` text,
  `title` varchar(255) DEFAULT NULL,
  `en_title` varchar(255) DEFAULT NULL,
  `link` varchar(5000) DEFAULT NULL,
  `link_target` varchar(20) DEFAULT NULL,
  `title_status` tinyint(3) DEFAULT '0',
  `link_status` tinyint(3) DEFAULT '0',
  `description_status` tinyint(1) DEFAULT '0',
  `ordering` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,2,'conheça nossa empresa',NULL,'10312c77531c72deb4254e2904821cd4.png',NULL,NULL,325,330,NULL,NULL,NULL,NULL,'','','',NULL,'',NULL,NULL,NULL,NULL,NULL,0,0,0,0,'2016-11-04 11:17:04','2016-11-04 11:17:18');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informations`
--

DROP TABLE IF EXISTS `informations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `email_two` varchar(255) NOT NULL,
  `email_three` varchar(255) NOT NULL,
  `telphone` varchar(20) NOT NULL,
  `telphone_two` varchar(20) DEFAULT NULL,
  `telphone_three` varchar(25) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `address` text NOT NULL,
  `number` int(11) NOT NULL,
  `complement` varchar(255) DEFAULT NULL,
  `neighborhood` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `telphone2` varchar(20) NOT NULL,
  `address2` varchar(255) NOT NULL,
  `number2` int(11) NOT NULL,
  `complement2` varchar(255) NOT NULL,
  `neighborhood2` varchar(255) NOT NULL,
  `city2` varchar(255) NOT NULL,
  `state2` varchar(255) NOT NULL,
  `maps2` varchar(5000) NOT NULL,
  `zipcode2` varchar(10) NOT NULL,
  `youtube` varchar(500) NOT NULL,
  `instagram` varchar(500) NOT NULL,
  `googleplus` varchar(500) NOT NULL,
  `facebook` varchar(500) NOT NULL,
  `linkedin` varchar(255) DEFAULT NULL,
  `twitter` varchar(500) NOT NULL,
  `maps` varchar(5000) NOT NULL,
  `link_externo` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informations`
--

LOCK TABLES `informations` WRITE;
/*!40000 ALTER TABLE `informations` DISABLE KEYS */;
INSERT INTO `informations` VALUES (1,'rcn@rcn.com.br','rcn@rcn.com.br','rcn@rcn.com.br','(11) 2548-0212','(11) 2092-2494','(11) 2092-2494','00000-000','Rua Heloisa Penteado',123,'','Guilhermina','São Paulo','SP','Brazil','','',0,'','','','','','','https://www.youtube.com/c1terapeutica','https://www.instagram.com/c1terapeutica','','https://www.facebook.com/c1terapeutica','http://linkedin.com','https://www.twitter.com/c1terapeutica','https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.8260725903265!2d-46.67920938529313!3d-23.538757266642445!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce57f161eca8d9%3A0x951ac6596eab0da8!2sR.+Vanderlei%2C+1107+-+Vila+Pompeia%2C+S%C3%A3o+Paulo+-+SP%2C+05011-001!5e0!3m2!1spt-BR!2sbr!4v1469192384874','https://hotels.cloudbeds.com/pt-br/reservas/Uakwm8','2016-03-04 00:00:00','2016-11-04 10:51:59');
/*!40000 ALTER TABLE `informations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `code` char(2) NOT NULL,
  `flag` varchar(3) NOT NULL,
  `ordering` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'Português','pt_BR','br','br',1,'2016-06-02 00:00:00','2016-06-02 00:00:00');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_categories`
--

DROP TABLE IF EXISTS `news_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `name` varchar(180) DEFAULT '',
  `en_name` varchar(180) DEFAULT '',
  `en_description` text,
  `en_resume` text,
  `src` varchar(255) DEFAULT '',
  `subtitle` varchar(150) DEFAULT '',
  `title` varchar(150) DEFAULT '',
  `description` text,
  `ranking` int(11) DEFAULT '0',
  `quantidade` int(11) DEFAULT '0',
  `resume` text,
  `category_date` int(4) DEFAULT '0',
  `ordering` int(11) DEFAULT '0',
  `created` date NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_categories`
--

LOCK TABLES `news_categories` WRITE;
/*!40000 ALTER TABLE `news_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_configs`
--

DROP TABLE IF EXISTS `news_configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_configs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `allow_content` tinyint(1) DEFAULT '0',
  `allow_category` int(11) DEFAULT '0',
  `allow_title` tinyint(1) DEFAULT '0',
  `allow_subtitle` tinyint(1) DEFAULT NULL,
  `allow_atributos` tinyint(1) DEFAULT '0',
  `allow_email` tinyint(1) DEFAULT '0',
  `allow_category_title` tinyint(1) DEFAULT '0',
  `allow_category_image` tinyint(1) DEFAULT '0',
  `allow_category_date` tinyint(1) DEFAULT '0',
  `allow_category_description` tinyint(1) DEFAULT '0',
  `allow_category_subtitle` tinyint(1) DEFAULT '0',
  `allow_category_resume` tinyint(1) DEFAULT '0',
  `allow_ordering` tinyint(1) DEFAULT '0',
  `allow_quantity` tinyint(1) DEFAULT '0',
  `allow_telphone` tinyint(1) DEFAULT '0',
  `allow_fax` tinyint(1) DEFAULT '0',
  `allow_link` int(11) DEFAULT '0',
  `allow_video` tinyint(1) DEFAULT '0',
  `allow_image` tinyint(1) DEFAULT NULL,
  `allow_thumb` tinyint(1) DEFAULT '0',
  `allow_auth` tinyint(1) DEFAULT '0',
  `allow_name` tinyint(1) DEFAULT '0',
  `allow_cargo` tinyint(1) DEFAULT '0',
  `allow_data` tinyint(1) DEFAULT '0',
  `allow_periodo` smallint(1) DEFAULT '0',
  `allow_periodo2` smallint(1) DEFAULT '0',
  `allow_valor` smallint(1) DEFAULT '0',
  `allow_valor2` smallint(1) DEFAULT '0',
  `allow_chamada` tinyint(1) DEFAULT '0',
  `allow_featured` tinyint(1) DEFAULT '0',
  `allow_symbol` tinyint(1) DEFAULT '0',
  `allow_question` smallint(1) DEFAULT '0',
  `image_width` int(11) DEFAULT NULL,
  `image_height` int(11) DEFAULT NULL,
  `thumb_width` int(11) DEFAULT NULL,
  `thumb_height` int(11) DEFAULT NULL,
  `crop_width` int(11) DEFAULT NULL,
  `crop_height` int(11) DEFAULT NULL,
  `crop_x` varchar(25) DEFAULT NULL,
  `crop_y` varchar(25) DEFAULT NULL,
  `image_category_width` int(11) DEFAULT NULL,
  `image_category_height` int(11) DEFAULT NULL,
  `thumb_category_width` int(11) DEFAULT NULL,
  `thumb_category_height` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_configs`
--

LOCK TABLES `news_configs` WRITE;
/*!40000 ALTER TABLE `news_configs` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_configs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_pages`
--

DROP TABLE IF EXISTS `news_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT '0',
  `featured` tinyint(1) DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `subtitle` varchar(255) DEFAULT NULL,
  `content` text,
  `en_title` varchar(255) DEFAULT '',
  `en_subtitle` varchar(155) DEFAULT '',
  `en_content` text,
  `es_title` varchar(255) DEFAULT '',
  `es_subtitle` varchar(255) DEFAULT '',
  `es_content` text,
  `valor` varchar(20) DEFAULT '',
  `valor2` varchar(20) DEFAULT '',
  `en_valor` varchar(20) DEFAULT '',
  `en_valor2` varchar(20) DEFAULT '',
  `es_valor` varchar(20) DEFAULT '',
  `es_valor2` varchar(20) DEFAULT '',
  `question` varchar(255) DEFAULT '',
  `en_question` varchar(255) DEFAULT '',
  `es_question` varchar(255) DEFAULT '',
  `answer` text,
  `en_answer` text,
  `es_answer` text,
  `periodo` varchar(50) DEFAULT NULL,
  `periodo2` varchar(50) DEFAULT NULL,
  `en_periodo` varchar(50) DEFAULT NULL,
  `en_periodo2` varchar(50) DEFAULT NULL,
  `es_periodo` varchar(50) DEFAULT NULL,
  `es_periodo2` varchar(50) DEFAULT NULL,
  `chamada` varchar(255) DEFAULT '',
  `en_chamada` varchar(255) DEFAULT '',
  `email` varchar(255) DEFAULT '',
  `telphone` varchar(25) DEFAULT '',
  `fax` varchar(20) DEFAULT '',
  `src` varchar(255) DEFAULT NULL,
  `src2` varchar(255) DEFAULT '',
  `alt` varchar(150) DEFAULT NULL,
  `atributos` text,
  `en_atributos` text,
  `es_atributos` text,
  `link` varchar(255) DEFAULT '',
  `video` varchar(255) DEFAULT '',
  `name` varchar(180) DEFAULT '',
  `slug` varchar(140) DEFAULT NULL,
  `auth` varchar(255) DEFAULT '',
  `cargo` varchar(140) DEFAULT '',
  `en_cargo` varchar(140) DEFAULT '',
  `es_cargo` varchar(180) DEFAULT '',
  `ordering` int(11) DEFAULT NULL,
  `ranking` int(11) DEFAULT '0',
  `quantidade` int(11) DEFAULT '0',
  `symbol` varchar(2) DEFAULT '',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `data` varchar(30) DEFAULT '',
  `en_data` varchar(30) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_pages`
--

LOCK TABLES `news_pages` WRITE;
/*!40000 ALTER TABLE `news_pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletters`
--

DROP TABLE IF EXISTS `newsletters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletters`
--

LOCK TABLES `newsletters` WRITE;
/*!40000 ALTER TABLE `newsletters` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `allow_create` tinyint(1) DEFAULT '0',
  `ctp` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(60) DEFAULT NULL,
  `description` varchar(155) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `robots` varchar(100) DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,0,'home','Home','','','','index, follow',0,'2016-11-04 10:52:19','2016-11-04 10:52:19'),(2,0,'empresa','Empresa','','','','index, follow',1,'2016-11-04 11:08:31','2016-11-04 11:08:31');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sectors`
--

DROP TABLE IF EXISTS `sectors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sectors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `setor` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `assunto` varchar(255) NOT NULL,
  `created` date NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sectors`
--

LOCK TABLES `sectors` WRITE;
/*!40000 ALTER TABLE `sectors` DISABLE KEYS */;
/*!40000 ALTER TABLE `sectors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(155) DEFAULT NULL,
  `google_analytics` text,
  `smtp_port` varchar(255) NOT NULL,
  `smtp_timeout` int(2) NOT NULL,
  `smtp_host` varchar(255) NOT NULL,
  `smtp_user` varchar(255) NOT NULL,
  `smtp_pass` varchar(5000) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'conta SMTP','null','conta smtp email do cliente','null','587',30,'smtp.hostelgrapewine.com.br','hostelgrapewine@hostelgrapewine.com.br','azsxdc321',1,'2016-03-04 00:00:00','2016-08-23 17:03:24');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `showcase_categories`
--

DROP TABLE IF EXISTS `showcase_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `showcase_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT '0',
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `showcase_categories`
--

LOCK TABLES `showcase_categories` WRITE;
/*!40000 ALTER TABLE `showcase_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `showcase_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `showcase_products`
--

DROP TABLE IF EXISTS `showcase_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `showcase_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `showcase_category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `code` varchar(11) DEFAULT NULL,
  `model` varchar(40) DEFAULT NULL,
  `specifications` text,
  `results` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `showcase_products`
--

LOCK TABLES `showcase_products` WRITE;
/*!40000 ALTER TABLE `showcase_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `showcase_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `showcase_products_images`
--

DROP TABLE IF EXISTS `showcase_products_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `showcase_products_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `showcase_product_id` int(11) NOT NULL,
  `src` varchar(255) NOT NULL,
  `alt` varchar(150) DEFAULT NULL,
  `longdesc` text,
  `featured` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `showcase_products_images`
--

LOCK TABLES `showcase_products_images` WRITE;
/*!40000 ALTER TABLE `showcase_products_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `showcase_products_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_videos`
--

DROP TABLE IF EXISTS `site_videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_videos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_videos`
--

LOCK TABLES `site_videos` WRITE;
/*!40000 ALTER TABLE `site_videos` DISABLE KEYS */;
/*!40000 ALTER TABLE `site_videos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `system_users`
--

DROP TABLE IF EXISTS `system_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `phonecel` varchar(20) DEFAULT NULL,
  `phonefax` varchar(20) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `add_product` tinyint(4) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `first_login` tinyint(1) DEFAULT '0',
  `tokenhash` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `system_users`
--

LOCK TABLES `system_users` WRITE;
/*!40000 ALTER TABLE `system_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `talents`
--

DROP TABLE IF EXISTS `talents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `talents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(220) DEFAULT NULL,
  `email` varchar(220) DEFAULT NULL,
  `sexo` tinyint(1) DEFAULT NULL,
  `nascimento` date DEFAULT NULL,
  `cpf` varchar(18) DEFAULT NULL,
  `civil` varchar(30) DEFAULT NULL,
  `filhos` varchar(60) DEFAULT NULL,
  `cep` varchar(22) DEFAULT NULL,
  `endereco` varchar(180) DEFAULT NULL,
  `complemento` varchar(100) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `celular` varchar(20) DEFAULT NULL,
  `tel_recado` varchar(20) DEFAULT NULL,
  `pessoa_recado` varchar(160) DEFAULT NULL,
  `interesse` varchar(160) DEFAULT NULL,
  `cargo` varchar(70) DEFAULT NULL,
  `area_trabalho` varchar(80) DEFAULT NULL,
  `salario` varchar(50) DEFAULT NULL,
  `escolaridade` varchar(50) DEFAULT NULL,
  `curso` varchar(50) DEFAULT NULL,
  `instituicao` varchar(50) DEFAULT NULL,
  `ano_conclusao` varchar(30) DEFAULT NULL,
  `empresa` varchar(80) DEFAULT NULL,
  `area_empresa` varchar(50) DEFAULT NULL,
  `cargo_empresa` varchar(50) DEFAULT NULL,
  `data_entrada` varchar(15) DEFAULT NULL,
  `data_saida` varchar(15) DEFAULT NULL,
  `motivo_saida` varchar(300) DEFAULT NULL,
  `empresa2` varchar(80) DEFAULT NULL,
  `area_empresa2` varchar(50) DEFAULT NULL,
  `cargo_empresa2` varchar(50) DEFAULT NULL,
  `data_entrada2` varchar(15) DEFAULT NULL,
  `data_saida2` varchar(15) DEFAULT NULL,
  `motivo_saida2` varchar(300) DEFAULT NULL,
  `empresa3` int(100) DEFAULT NULL,
  `area_empresa3` int(50) DEFAULT NULL,
  `cargo_empresa3` int(50) DEFAULT NULL,
  `data_entrada3` varchar(15) DEFAULT NULL,
  `data_saida3` varchar(15) DEFAULT NULL,
  `motivo_saida3` varchar(300) DEFAULT NULL,
  `curso2` varchar(50) DEFAULT NULL,
  `instituicao2` varchar(100) DEFAULT NULL,
  `duracao2` varchar(80) DEFAULT NULL,
  `curso3` varchar(100) DEFAULT NULL,
  `instituicao3` varchar(100) DEFAULT NULL,
  `duracao3` varchar(80) DEFAULT NULL,
  `curso4` varchar(100) DEFAULT NULL,
  `instituicao4` varchar(100) DEFAULT NULL,
  `duracao4` varchar(80) DEFAULT NULL,
  `idioma` text,
  `portador_necessidade` varchar(200) DEFAULT NULL,
  `comentarios` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `talents`
--

LOCK TABLES `talents` WRITE;
/*!40000 ALTER TABLE `talents` DISABLE KEYS */;
/*!40000 ALTER TABLE `talents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `texts`
--

DROP TABLE IF EXISTS `texts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `texts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL DEFAULT 'textarea',
  `label` varchar(45) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `en_title` varchar(255) DEFAULT '',
  `es_title` varchar(255) DEFAULT '',
  `en_label` varchar(255) DEFAULT '',
  `es_label` varchar(255) DEFAULT '',
  `subtitle` varchar(255) DEFAULT '',
  `en_subtitle` varchar(255) DEFAULT '',
  `es_subtitle` varchar(255) DEFAULT '',
  `content` text,
  `en_content` text,
  `es_content` text,
  `required` tinyint(4) DEFAULT '0',
  `ordering` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `texts`
--

LOCK TABLES `texts` WRITE;
/*!40000 ALTER TABLE `texts` DISABLE KEYS */;
INSERT INTO `texts` VALUES (1,1,'textarea','conheça nossa empresa','conheça nossa empresa','','','','','sobre Nós','','','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pharetra, purus a tincidunt lobortis, purus dolor iaculis metus, sed faucibus nisl orci ut risus. Donec sed dignissim ligula. Nullam tempus velit orci, sed finibus dui rutrum finibus. Donec egestas ligula sed nulla pretium, in lobortis ligula pharetra. Curabitur felis purus, fringilla vel lorem pretium, vulputate gravida urna. Donec egestas ligula sed nulla pretium, in lobortis ligula pharetra. Curabitur felis purus, fringilla vel lorem pretium, vulputate gravida urna.',NULL,NULL,0,0,'2016-11-04 10:52:39','2016-11-04 10:54:12'),(2,1,'text','conheça nossos produtos','conheça nossos produtos','','','','','PRODUTOS','','','',NULL,NULL,0,1,'2016-11-04 10:57:39','2016-11-04 10:57:45'),(3,1,'text','conheça nossos eventos','conheça nossos eventos','','','','','EVENTOS','','','',NULL,NULL,0,2,'2016-11-04 10:58:28','2016-11-04 11:01:44'),(4,2,'ckeditor','conheça nossa empresa','conheça nossa empresa ','','','','','SOBRE NÓS','','','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pharetra, purus a tincidunt lobortis, purus dolor iaculis metus, sed faucibus nisl orci ut risus. Donec sed dignissim ligula. Nullam tempus velit orci, sed finibus dui rutrum finibus. Donec egestas ligula sed nulla pretium, in lobortis ligula pharetra. Curabitur felis purus, fringilla vel lorem pretium, vulputate gravida urna. Donec egestas ligula sed nulla pretium, in lobortis ligula pharetra.<br />\r\nCurabitur felis purus, fringilla vel lorem pretium, vulputate gravida urna.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pharetra, purus a tincidunt lobortis, purus dolor iaculis metus, sed faucibus nisl orci ut risus.<br />\r\nDonec sed dignissim ligula. Nullam tempus velit orci, sed finibus dui rutrum finibus. Donec egestas ligula sed nulla pretium, in lobortis ligula pharetra. Curabitur felis purus, fringilla vel lorem pretium, vulputate gravida urna. Donec egestas ligula sed nulla pretium, in lobortis ligula pharetra. Curabitur felis purus, fringilla vel lorem pretium, vulputate gravida urna</p>\r\n',NULL,NULL,0,3,'2016-11-04 11:08:43','2016-11-04 11:18:40'),(6,2,'textarea','visão','visão','','','','','','','','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pharetra, purus a tincidunt lobortis, purus dolor iaculis metus.',NULL,NULL,0,5,'2016-11-04 11:11:33','2016-11-04 11:12:27'),(7,2,'textarea','missão','missão','','','','','','','','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pharetra, purus a tincidunt lobortis, purus dolor iaculis metus.',NULL,NULL,0,6,'2016-11-04 11:13:12','2016-11-04 11:13:32'),(8,2,'textarea','valores','valores','','','','','','','','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pharetra, purus a tincidunt lobortis, purus dolor iaculis metus.',NULL,NULL,0,7,'2016-11-04 11:13:51','2016-11-04 11:13:57');
/*!40000 ALTER TABLE `texts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `first_login` tinyint(1) NOT NULL,
  `tokenhash` varchar(255) DEFAULT NULL,
  `debug` int(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Desenvolvedor','dev.png','desenvolvedor@foxsolution.com.br','5fbd0228d934a875f5abbef211fdaf21eb697b63','super_user',1,0,'d7e50a580756ee40e1ec5a339232d80795e59ac9',2,1,'2016-03-04 09:39:00','2016-11-04 10:47:18'),(2,'admin',NULL,'administrador@foxsolution.com.br','5fbd0228d934a875f5abbef211fdaf21eb697b63','admin',1,0,'ee6bfef0ce56074b5cbcd8c9c9763b59a0e53557',0,1,'2016-06-28 12:53:55','2016-09-13 21:42:36');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'globalmoema'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-22 16:22:17
